//
// Created by KJ Patterson on 2/22/17.
//

#include "Card.h"
#include <iomanip>

Card::Card(int cardSize, int numberMax, int cardIndex)
{
     m_cardSize = cardSize;
     m_numberMax = numberMax;
     m_cardIndex = cardIndex;

    for (int i = 1; i <= m_numberMax; i++)
    {
        createCard.push_back(i);
    }
    std::random_shuffle(createCard.begin(), createCard.end());
    for (int j = 0; j < m_cardSize; j++)
    {
        std::vector <int> tempCard;
        for (int k = 0; k < m_cardSize; k++)
        {
            tempCard.push_back(createCard[k]);
            createCard.erase(createCard.begin() + k);
        }
        finalBingoCard.push_back(tempCard);
    }
}

void Card::printCard(std::ostream &out)
{
    std::cout << "Card # " << m_cardIndex << std::endl;

    for (int m = 0; m < m_cardSize; m++)
    {
        for (int i = 0; i < m_cardSize; i++)
        {
            out << "+---+";
        }
        out << std::endl;
        for (int j = 0; j < m_cardSize; j++)
        {
            out  << "|"  << std::setw(3) << finalBingoCard[m][j] << "|";
        }
        std::cout << std::endl;
    }
    out << std::endl;
}
