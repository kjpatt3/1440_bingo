//
// Created by KJ Patterson on 2/22/17.
//

#ifndef BINGO_CARDS_H
#define BINGO_CARDS_H
#include <fstream>
#include <iostream>
#include <vector>


class Card
{
public:
    void printCard(std::ostream& out);
    Card (int cardSize, int numberMax, int cardIndex);
    friend class CardTester;
    friend class DeckTester;

private:
    int m_cardSize;
    int m_numberMax;
    int m_cardIndex;
    std::vector <int> createCard;
    std::vector<std::vector <int>> finalBingoCard;


};


#endif //BINGO_CARDS_H
