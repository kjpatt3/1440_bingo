//
// Created by KJ Patterson on 3/1/17.
//

#include "DeckTester.h"
#include "../Deck.h"



void DeckTester::testDeckConstructor()
{
    std:: cout << "Test Suite: Deck Tester" << std::endl;
    std::cout << "Test Case 1: Testing for Match in Input." << std::endl;
    Deck testDeck(3,5,18);
    if (testDeck.m_cardSize != 3)
    {
        std::cout << "Unexpected Error in Card Size Match." << std::endl;
    }
    else if (testDeck.m_cardCount != 5)
    {
        std::cout << "Unexpected Error in Card Count Match." << std::endl;
    }
    else if (testDeck.m_numberMax != 18)
    {
        std::cout << "Unexpected Error in Max Number" << std::endl;
    }
}

void DeckTester::testForDuplicateCards()
{
    std::cout << "Test Case 2: Testing for Duplicate Cards." << std::endl;
    Deck deck(3, 3, 18);
    // int cardSize, int cardCount, int numberMax
    Card card(3, 18, 1);
   // int cardSize, int numberMax, int cardIndex

    std::vector<std::vector <int>> testBingoCard;

    testBingoCard = card.finalBingoCard;
   if (card.m_cardSize != deck.m_cardSize)
   {
       std::cout << "Your Card Size's do not match." << std::endl;
   }
    if (card.m_numberMax != deck.m_numberMax)
    {
        std::cout << "Your Max Number's do not match." << std::endl;
    }
  if (card.m_cardSize == deck.m_cardSize && card.m_numberMax != deck.m_numberMax)
  {
      for (int i = 0; i < deck.m_cardCount; i++)
      {
          for (int k = 0; k < deck.m_cardCount; k++)
          {
             if (testBingoCard[i][k] == testBingoCard[i + 1][k + 1])
             {
                 std::cout << "Duplicate Cards found." << std::endl;
             }
          }
      }
  }
}


