//
// Created by Stephen Clyde on 2/20/17.
//

#include <iostream>


#include "MenuOptionTester.h"
#include "MenuTester.h"
#include "CardTester.h"
#include "DeckTester.h"

int main()
{

 // Initialize the random number generator
 unsigned int seed = (unsigned int) time(NULL);
 srand(seed);


 MenuOptionTester menuOptionTester;
 menuOptionTester.testConstructorAndGetter();

 MenuTester menuTester;
 menuTester.testConstructorAndGetter();

 std::cout << std::endl;

 CardTester cardTester;
 cardTester.testForDuplicates();
 cardTester.testingForZeroOrNegative();
 cardTester.testMaxNumber();
 cardTester.testConstructor();

    std::cout << std::endl;

 DeckTester deckTester;
 deckTester.testDeckConstructor();
 deckTester.testForDuplicateCards();
}