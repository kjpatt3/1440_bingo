//
// Created by KJ Patterson on 2/28/17.
//

#ifndef BINGO_CARDTESTER_H
#define BINGO_CARDTESTER_H


class CardTester
{

public:
    void testForDuplicates();
    void testingForZeroOrNegative();
    void testMaxNumber();
    void testConstructor();

};


#endif //BINGO_CARDTESTER_H
