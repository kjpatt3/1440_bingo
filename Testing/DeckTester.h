//
// Created by KJ Patterson on 3/1/17.
//

#ifndef BINGO_DECKTESTER_H
#define BINGO_DECKTESTER_H


class DeckTester
{
public:
    void testDeckConstructor();
    void testForDuplicateCards();
};


#endif //BINGO_DECKTESTER_H
