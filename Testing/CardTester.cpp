//
// Created by KJ Patterson on 2/28/17.
//

#include "CardTester.h"
#include "../Card.h"


void CardTester::testForDuplicates()
{
    std:: cout << "Test Suite: Card Tester" << std::endl;

    std::cout << "Test Case 1: Testing for Duplicate Numbers." << std::endl;
    Card card(3, 18, 1);
    std::vector<int> testCard;

    testCard = card.createCard;

    std::sort(testCard.begin(), testCard.end());

    for (int i = 0; i < testCard.size(); i++)
    {
        if (testCard[i] == testCard[i + 1])
        {
            std::cout << "Duplicate numbers found." << std::endl;
        }
    }
}

void CardTester::testingForZeroOrNegative()
{
    std::cout << "Test Case 2: Testing for 0." << std::endl;
    Card zeroCard(0, 0, 0);
    std::vector<int> testCard;

    testCard = zeroCard.createCard;

    std::sort(testCard.begin(), testCard.end());

    for (int i = 0; i < testCard.size(); i++)
    {
        if (testCard[i] == 0)
        {
            std::cout << "Unexpected Number on Card. Number cannot be Less than or Equal to 0" << std::endl;
        }
    }
    std::cout << "Test Case 3: Negative Numbers." << std::endl;
    Card negativeCard(-2, -1, -4);
    std::vector<int> card1;

    card1 = negativeCard.createCard;

    std::sort(card1.begin(), card1.end());

    for (int i = 0; i < card1.size(); i++)
    {
        if (card1[i] <= 0)
        {
            std::cout << "Unexpected Number on Card. Number cannot be Less than or Equal to 0" << std::endl;
        }
    }
    std::cout << "Test Case 4: Testing for 0 And Negative Numbers." << std::endl;
    Card mixedCard(0, -1, 0);
    std::vector<int> card2;

    card2 = mixedCard.createCard;

    std::sort(card2.begin(), card2.end());

    for (int i = 0; i < card2.size(); i++)
    {
        if (card2[i] <= 0)
        {
            std::cout << "Unexpected Number on Card. Number cannot be Less than or Equal to 0" << std::endl;
        }
    }
}

void CardTester::testMaxNumber()
{
    std::cout << "Test Case 5: Testing for maximum number." << std::endl;
    Card card(3, 18, 1);
    std::vector<int> testCard;

    testCard = card.createCard;

    std::sort(testCard.begin(), testCard.end());

    for (int i = 0; i < testCard.size(); i++)
    {
        if (testCard[i] >card.m_numberMax)
        {
            std::cout << "Invalid Number. Number is greater than maximum number allowed." << std::endl;
        }
    }
}

void CardTester::testConstructor()
{
    std::cout << "Test Case 6: Testing to Ensure Input's Match." << std::endl;
    Card testCard(3,18,1);
    if (testCard.m_cardIndex != 1)
    {
        std::cout << "Unexpected Error in Card Index" << std::endl;
    }
    else if (testCard.m_cardSize != 3)
    {
        std::cout << "Unexpected Error in Card Size" << std::endl;
    }
    else if (testCard.m_numberMax != 18)
    {
        std::cout << "Unexpected Error in Max Number." << std::endl;
    }
}




