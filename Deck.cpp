//
// Created by Stephen Clyde on 2/16/17.
//

#include "Deck.h"
#include "Card.h"


Deck::Deck(int cardSize, int cardCount, int numberMax)
{
    m_cardSize = cardSize;
    m_cardCount = cardCount;
    m_numberMax = numberMax;

  for (int i = 1; i <= m_cardCount; i++)
  {
      Card* newCard = new Card(cardSize, numberMax, i);
      deck.push_back(newCard);
  }
}

Deck::~Deck()
{
    for (int i = 0; i < m_cardCount; i++)
    {
        delete deck[i];
    }
}


void Deck::print(std::ostream& out) const
{
    for (int i = 0; i < m_cardCount; i++)
    {
        deck[i]->printCard(out);
    }
}

void Deck::print(std::ostream& out, int cardIndex) const
{
    deck[cardIndex - 1]->printCard(out);
}





